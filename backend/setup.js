// CreatesDB, sets it up and adds a few users + posts
//
// mongod --dbpath=./dev/twatter/backend/database

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Database Name
const dbName = 'twatterdb';

// Connection URL
const url = 'mongodb://localhost:27017';

// Use connect method to connect to the server
MongoClient.connect(url, { useNewUrlParser:true } ,function (err, client) {
    assert.equal(null, err);
    console.log("Connected to db");
    const db = client.db(dbName);
    setupDB(db, function () {
        client.close();
        exitNode();
    });
});


// Exit node script
const exitNode = function () {
    process.exit();
}


// Create the user document, set username & email to be unique
const setupDB = function (db, callback) {
    //Get the documents collection
    const collection = db.collection('user');
    collection.createIndex({ "nickname":1}, { unique: true })
    collection.createIndex({ "email": 1 }, { unique: true })
    collection.createIndex({ "token": 1}, {unique: true })

    const collection2 = db.collection('resets');
    collection2.createIndex({ "token": 1}, { unique: true })
}