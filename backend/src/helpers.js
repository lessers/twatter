const crypto = require('crypto')
const nodemailer = require('nodemailer')

exports.token = callback => crypto.randomBytes(32, (err, buffer) => callback(buffer.toString('hex')))

exports.sendMail = (to, subject, text) => {
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'twattertech@gmail.com',
      pass: 'encryptedpassword'
    }
  })

  var mailOptions = {
    from: 'twattertech@gmail.com',
    to,
    subject,
    text,
  }

  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        reject(error)
      } else {
        resolve(info.response)
      }
    })
  })
}
