const express = require('express')
const router = express.Router()

const bcrypt = require('bcrypt');
const { token, sendMail } = require('./helpers')
const { ObjectId } = require('mongodb')

const HTTP_OK = 200
const HTTP_BAD_REQUEST = 400
const HTTP_UNAUTHORIZED = 401
const HTTP_FORBIDDEN = 403

async function auth(req, res, next) {
  let header = req.get('Authorization')
  
  if (!header) {
    // Not signed in
    return next()
  }

  // Remove 'Bearer' from header.
  let inc_token = header.substring(7)

  // Expiry time, last activity must be greater.
  let expiration = new Date()
  expiration.setHours(expiration.getHours() - 1)

  // Find user and refresh session lastActivity.
  let user = await req.db.collection('user').findOneAndUpdate({
    token: inc_token,
    lastActivity: { $gt: +expiration },
  }, {
    $set: {
      lastActivity: +new Date(),
    },
  })

  user = user.value

  // Check that we found a user with the given token.
  if (user==null) {
    return next()
  }

  req.user = user

  next()
}

function requireAuth(req, res, next) {
  if (req.user) return next()

  res.sendStatus(HTTP_UNAUTHORIZED)
}

router.post('/register', async (req, res) => {
  let {email, nickname, password} = req.body

  let nickExists = await req.db.collection('user').count({nickname: nickname})
  let emailExists = await req.db.collection('user').count({email: email})

  if (emailExists > 0 || nickExists > 0) {
    res.status(HTTP_BAD_REQUEST)
    return res.json({
      error: (emailExists > 0) ? 'Email already exists.' : 'Nickname already exists.',
    })
  }

  // Validate email
  let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (!re.test(String(email).toLowerCase())) {
    console.log("bad email")
    res.status(HTTP_BAD_REQUEST)
    return res.json({
      error: 'Invalid email.',
    })
  }

  // Validate password
  if (password.length < 12) {
    console.log("Bad password")
    res.status(HTTP_BAD_REQUEST)
    return res.json({
      error: 'Password too short.',
    })
  }

  // Create user, hash password
  var saltRounds = 10;
  var salt = await bcrypt.genSalt(saltRounds);
  var hash = await bcrypt.hash(password, salt);

  token(async token => {
    // Register and sign in user.
    try {
      await req.db.collection('user').insertOne({
        email,
        nickname,
        password: hash,
        token,
        lastActivity: +new Date(),
      })
      // Respond with token and user.
      res.json({
        data: {
          token,
          user: {
            nickname: nickname,
          },
        }
      })
    } catch (e) {
      res.status(HTTP_BAD_REQUEST)
      return res.json({
        error: 'Something went wrong, but we\'re not notified and probably won\'t fix it.',
      })
    }
  })
})

router.post('/login', async (req, res) => {
  let {email, password} = req.body
  
  let user = await req.db.collection('user').findOne({email: email})
  token(newtoken => {
    bcrypt.compare(password, user.password).then(async match => { 
      if (match) {
        // Respond with token and user.
        try {
          let result = await req.db.collection('user').findOneAndUpdate({
            _id: user._id,
          }, {
            $set:{token: newtoken, lastActivity: +new Date()},
          })
          res.status(HTTP_OK)
          res.json({
            data: {
              token:  newtoken,
              user: {nickname: user.nickname},
            },
          })
        }
        catch (e) {
          res.status(HTTP_BAD_REQUEST)
          return res.json({
            error: 'Something went wrong, but we\'re not notified and probably won\'t fix it.',
          })
        }
      } 
      
      else {
        res.status(HTTP_UNAUTHORIZED)
        res.json({
          error: 'Invalid email or password.',
        })
      }
    })
  })
})

router.delete('/logout', requireAuth, async(req, res) => {
  // req.user
  
  let result = await req.db.collection('user').findOneAndUpdate({
    _id: req.user._id,
  }, {
    $set:{token: null, lastActivity: +new Date()},
  })
  // Delete session by user id

  res.sendStatus(HTTP_OK)
})

router.post('/reset-token', async (req, res) => {
  let { email } = req.body

  // Validate email exists
  let user = await req.db.collection('user').findOne({ email })
  console.log(email, user)

  if (!user) {
    res.status(HTTP_BAD_REQUEST)
    return res.json({
      error: 'The email was not found',
    })
  }

  // Generate token
  // Send email with token
  token(async token => {
    try {
      await req.db.collection('resets').insertOne({
        userId: user._id,
        token,
        createdAt: +new Date,
      })
  
      await sendMail(
        email,
        'Reset your password',
        `Reset your password here: http://localhost:3000/reset-password/${token}`
      )
  
      res.sendStatus(HTTP_OK)
    } catch (e) {
      res.status(HTTP_BAD_REQUEST)
      return res.json({
        error: 'Something went wrong, but we\'re not notified and probably won\'t fix it.',
      })
    }
  })
})

router.post('/reset', async (req, res) => {
  let { resetToken, password } = req.body

  // Expiry time, createdAt must be greater.
  let expiration = new Date()
  expiration.setHours(expiration.getHours() - 1)

  let reset = await req.db.collection('resets').findOne({
    token: resetToken,
    createdAt: { $gt: +expiration },
  })

  // Validate resetToken and email, not expired
  if (!reset) {
    res.status(HTTP_BAD_REQUEST)
    return res.json({
      error: 'Your password reset code is invalid!',
    })
  }
  
  // Validate password
  if (password.length < 12) {
    res.status(HTTP_BAD_REQUEST)
    return res.json({
      error: 'Password too short.',
    })
  }

  let saltRounds = 10;
  let salt = await bcrypt.genSalt(saltRounds);
  let hash = await bcrypt.hash(password, salt);

  // Reset password and login user
  token(async token => {
    let user = await req.db.collection('user').findOneAndUpdate({
      _id: reset.userId
    }, {
      $set: {
        password: hash,
        token,
      }
    })

    await req.db.collection('resets').findOneAndUpdate({
      _id: reset._id,
    }, {
      $set: { token: null }
    })

    user = user.value
  
    res.json({
      data: {
        token: user.token,
        user: {
          nickname: user.nickname,
        },
      },
    })
  })
})

auth.routes = router
auth.requireAuth = requireAuth

module.exports = auth
