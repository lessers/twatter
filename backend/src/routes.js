const express = require('express')
const router = express.Router()
const { ObjectId } = require('mongodb')
const { requireAuth } = require('./auth')
const constants = require('./constants')

const bcrypt = require('bcrypt');

const HTTP_OK = 200
const HTTP_BAD_REQUEST = 400
const HTTP_UNAUTHORIZED = 401
const HTTP_FORBIDDEN = 403

router.get('/me', requireAuth, async (req, res) => {
  res.json({
    data: {
      user: {
        nickname: req.user.nickname,
      },
    },
  })
})

router.get('/tweets', async (req, res) => {
  // Filtering/searching/sorting is done client side.
  let tweets = await req.db.collection('tweets').aggregate([{
    $project: {
      owner: 1,
      text: 1,
      upVotes: { $size: '$upVotes' },
      downVotes: { $size: '$downVotes' },
      timestamp: 1,
    }
  }]).toArray()

  res.json({
    data: {
      tweets,
    },
  })
})

router.post('/tweets', requireAuth, async (req, res) => {

  let {text, nickname}= req.body

  let result = await req.db.collection('tweets').insertOne({
    userId: ObjectId(req.user._id),
    owner: nickname,
    text,
    upVotes: [],
    downVotes: [],
    timestamp: + new Date(),
  })

  let tweet = Object.assign({}, result.ops[0], {
    upVotes: 0,
    downVotes: 0,
  })

  return res.json({
    data: {
      tweet,
    },
  })
})

router.delete('/tweets/:tweetId', requireAuth, async (req, res) => {
  // A user can only remove their own tweets.
  try {
    let result = await req.db.collection('tweets').deleteOne({
      _id: ObjectId(req.params.tweetId),
      userId: req.user._id,
    })

    if (result.deletedCount === 0) {
      return res.sendStatus(HTTP_BAD_REQUEST)
    }

    res.sendStatus(HTTP_OK)
  } catch (e) {
    res.sendStatus(HTTP_BAD_REQUEST)
  }
})

router.post('/tweets/:tweetId/upvote', requireAuth, async (req, res) => {
  try {
    let result = await req.db.collection('tweets').findOneAndUpdate({
      _id: ObjectId(req.params.tweetId),
    }, {
      $addToSet: {
        upVotes: req.user.nickname,
      },
      $pull: {
        downVotes: req.user.nickname,
      },
    })

    res.sendStatus(HTTP_OK)
  } catch (e) {
    res.sendStatus(HTTP_BAD_REQUEST)
  }
})

router.post('/tweets/:tweetId/downvote', requireAuth, async (req, res) => {
  try {
    let result = await req.db.collection('tweets').updateOne({
      _id: ObjectId(req.params.tweetId),
    }, {
      $addToSet: {
        downVotes: req.user.nickname,
      },
      $pull: {
        upVotes: req.user.nickname,
      },
    })

    res.sendStatus(HTTP_OK)
  } catch (e) {
    res.sendStatus(HTTP_BAD_REQUEST)
  }
})

module.exports = router
