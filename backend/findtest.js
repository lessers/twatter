const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const ObjectId = require('mongodb').ObjectID;


// Database Name
const dbName = 'twatterdb';

// Connection URL
const url = 'mongodb://localhost:27017';

// Use connect method to connect to the server
MongoClient.connect(url, { useNewUrlParser: true }, function (err, client) {
    assert.equal(null, err);
    console.log("Connected to db");
    const db = client.db(dbName);
    findDocuments(db, () => { process.exit();});
});


// Exit node script
const exitNode = function () {
    process.exit();
}


// Create the user document, set username & email to be unique
const findDocuments = function (db, callback) {
    // Get the documents collection
    const collection = db.collection('user');
    // Find some documents
    collection.find({},{projection: {id: 1}}).toArray(function (err, docs) {
        assert.equal(err, null);
        console.log("Found the following records");
        console.log(docs)
        callback(docs);
    });
}