const express = require('express')

const app = express()
const cors = require('cors')

const MongoClient = require('mongodb').MongoClient
const bodyParser = require('body-parser')

const { token } = require('./src/helpers')
const auth = require('./src/auth')
const routes = require('./src/routes')

// Database Name
const dbName = 'twatterdb'

// Connection URL
//const opts = {db:{authSource:"twatterdb"}}
const url = 'mongodb://twatter:twatter@localhost:27017'

let db

app.use(cors())
app.use(bodyParser.json())

// Pass db in req so that all routes easily can access it.
app.use((req, res, next) => {
  req.db = db
  next()
})

app.use(auth)
app.use(auth.routes)
app.use(routes)

app.get('/token', (req, res) => token(t => res.send(t)))

app.get('/peter', (req, res) => res.send('Batman är Peteasdasd!'))

app.get('/users', async (req, res) => {
  let data = await req.db.collection('user').find().toArray()
  res.json(data)
})

app.get('/reset', (req, res) => sendMail())

// Use connect method to connect to the server
MongoClient.connect(url, { useNewUrlParser:true, db:{authSource:"twatterdb"} }, (err, client) => {
  console.log("Connected successfully to server")

  db = client.db(dbName)

  app.listen(3001, () => console.log('Listening on port 3001!'))
})
