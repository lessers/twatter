let makeFetch = sessionToken => (method, address, body) => fetch('http://localhost:3001' + address, {
        method,
        body,
        headers: new Headers({
            'Authorization': 'bearer ' + sessionToken,
            'Content-Type': 'application/json'
        })
    })

export { makeFetch }
