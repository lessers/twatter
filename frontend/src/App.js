import React, { Component } from 'react';
import logo from './lord.jpg';
import './App.css';
import Item from './Item.js'
import Search from 'material-ui/svg-icons/action/search'
import IconButton from 'material-ui/IconButton'
import { MuiThemeProvider } from 'material-ui/styles';
import { makeFetch } from './helpers'
let toke = window.localStorage.getItem('session-token')
let fetcher = makeFetch(toke)
class App extends Component {
  state = {
    title: 'Loading...',
    showLogin: false,
    showRegister: false,
    registerErrorMessage: '',
    loginErrorMessage: '',
    resetMessage: '',
    tweets: [ ]
  }
  componentDidMount() {
    fetcher('GET','/tweets')
    .then(r => r.json())
    .then(r => {
      this.setState({ tweets: this.state.tweets.concat(r.data.tweets), title: 'Welcome to Twattter™'})
    })
    this.setState({resetToken:/\/reset-password\/(\w+)$/.exec(window.location.pathname)})
    fetcher('GET', '/me')
    .then(res => res.status === 200 && res.json())
    .then(res => res && res.data && res.data.user && this.setState({user: res.data.user}) )
    window.onclick = event => {
      if(event.target === document.getElementById('login-screen'))this.setState({showLogin: false})
      else if(event.target === document.getElementById('register-screen')) this.setState({showRegister: false})
      else if(event.target === document.getElementById('new-message-screen')) this.setState({createNewTwat:false})
      else if(event.target === document.getElementById('reset-password-screen')) this.setState({resetToken:null})
    }
  }
  logout = ()=> {
    fetcher = makeFetch(null)
    window.localStorage.setItem('session-token', null)
    this.setState({user: null})
    window.location.reload()
  }
  login = event => {
    event.preventDefault()
    let data = {
      email: event.target.email.value,
      password: event.target.password.value
    }
    this.setState({loginErrorMessage:''})
    fetcher('POST', '/login', JSON.stringify(data))
    .then(res => {
      if(res.status === 200) return res.json()
      else if(res.status === 400) this.setState({loginErrorMessage: 'Invalid information supplied!'})
      else if(res.status === 401) this.setState({loginErrorMessage: 'Unauthorized access!'})
      else if(res.status === 403) this.setState({loginErrorMessage: 'Too many attempts!'})
      else this.setState({loginErrorMessage: 'Something went wrong. Please try again!'})
    })
    .catch(error => console.error('Error:', error))
    .then(response => {
      if(response) {
        fetcher = makeFetch(response.data.token)
        window.localStorage.setItem('session-token',response.data.token)
        this.setState({user: response.data.user})
      }
    })
  }
  register = event => {
    event.preventDefault()
    let data = {
      email: event.target.email.value,
      nickname: event.target.nickname.value,
      password: event.target.password.value
    }
    
    this.setState({registerErrorMessage: ''})
    fetcher('POST','/register', JSON.stringify(data))
    .then(res => {
      if(res.status === 200) return res.json()
      else if(res.status === 400) this.setState({registerErrorMessage: 'Something went wrong. Please try again!'})
      else this.setState({registerErrorMessage: 'Something went wrong. Please try again!'})
    })
    .catch(error => console.error('Error: ', error))
    .then(response => {
      if(response) {
        fetcher = makeFetch(response.data.token)
        window.localStorage.setItem('session-token',response.data.token)
        this.setState({user: response.data.user,showRegister: false})
      }
    })
  }
  sendResetMail = event => {
    event.preventDefault()
    fetcher('POST','/reset-token', JSON.stringify({email: event.target.email.value}))
    .then(()=> this.setState({resetMessage: 'Password has been reset!'}))
  }
  resetPassword = event => {
    event.preventDefault()
    if(event.target.newpassword.value === event.target.repeatpassword.value) {
      fetcher('POST','/reset', JSON.stringify({resetToken:this.state.resetToken[1],password: event.target.newpassword.value}))
      .then(res => {
        if(res.status === 200) return res.json()
        else this.setState({loginErrorMessage: 'Something went wrong. Please try again!'})
      })
      .then(response => {
        if(response) {
          fetcher = makeFetch(response.data.token)
          window.localStorage.setItem('session-token',response.data.token)
          this.setState({user: response.data.user,resetToken: null})
        }
      })
    }
  }
  saveTweetEdit = changedText => alert('saved')
  postNewMessage = event => {
    event.preventDefault()
    fetcher('POST','/tweets',JSON.stringify({text: event.target.message.value,nickname:this.state.user.nickname}))
    .then(res =>  (res.status === 200 && res.json()) || this.logout())
    .then(response => response.data && 
      this.setState({tweets: this.state.tweets.concat(response.data.tweet),createNewTwat: false})
    )
  }
  deleteTweet = deleteId => {
    fetcher('DELETE','/tweets/'+deleteId).then(res => res.status === 401 && this.logout()) 
    this.setState({tweets: this.state.tweets.filter(tweet=> deleteId !== tweet._id)})
  }
  searchTweets = event => {
    event.preventDefault()
    this.setState({filter:this.search.value,filterType:this.searchtype.value})
  }
  upVote = (tweetId, hasDownVoted) => { this.setState({tweets: this.state.tweets
      .map(tweet=> {
        if(tweet._id === tweetId) {
          if(!hasDownVoted) {
            tweet.hasUpVoted = true
            tweet.upVotes +=1
          } 
          else { 
            tweet.hasDownVoted = false
            tweet.hasUpVoted = true
            tweet.upVotes+=1 
            tweet.downVotes-=1
          }
        }
        return tweet
      })
    })
    fetcher('POST', '/tweets/'+tweetId+'/upvote')
  }
  downVote = (tweetId,hasUpVoted) => { this.setState({tweets: this.state.tweets
      .map(tweet => {
        if(tweet._id === tweetId) { 
          if(!hasUpVoted) { 
            tweet.downVotes+=1
            tweet.hasDownVoted = true 
          }
          else { 
            tweet.hasDownVoted = true
            tweet.hasUpVoted = false
            tweet.upVotes-=1 
            tweet.downVotes+=1
          }
        }
        return tweet
      })
    })
    fetcher('POST', '/tweets/'+tweetId+'/downvote')
  }
  dateSort = () => this.setState({
    tweets: this.state.tweets.sort((lefttweet,rightweet) => lefttweet.timestamp < rightweet.timestamp)
  })
  fameSort = () => this.setState({
    tweets: this.state.tweets.sort((left,right) => {
      let leftFame = left.upVotes - left.downVotes
      let rightFame = right.upVotes - right.downVotes
      return leftFame < rightFame
    })
  })
  render() {
     let { title} = this.state
    return (
      <div className="App">
        <MuiThemeProvider >
          <div>
            {this.state.resetToken &&
              (<div id="reset-password-screen" className="login-screen">
                <div className="login-box">
                  <form  onSubmit={this.resetPassword} >
                    <input name="newpassword" placeholder="Choose your new password.." type="password" />
                    <input name="repeatpassword" placeholder="Repeat password..." type="password" />
                    <button type="submit">RESET &amp; LOGIN</button>
                  </form>
                  <p>{this.state.resetPwdmessage}</p>
                </div>
              </div>)
              }
              { this.state.showLogin &&
                (<div id="login-screen" className="login-screen">
                  <div className="login-box">
                    <form  onSubmit={this.login} >
                      <input name="email" placeholder="E-mail" type="text" />
                      <input name="password" placeholder="Password..." type="password" />
                      <button type="submit">LOGIN</button>
                    </form>
                    <p>{this.state.loginErrorMessage}</p>
                    <form onSubmit={this.sendResetMail}>
                      <input name="email" placeholder="E-mail..." type="text" />
                      <button type="submit">RESET</button>
                    </form>
                    <p>{this.state.resetMessage}</p>
                  </div>
                </div>)
              }
              {this.state.showRegister &&
                (<div id="register-screen" className="login-screen">
                  <div className="login-box">
                    <form  onSubmit={this.register} >
                      <input name="nickname" placeholder="Nickname..." type="text" />
                      <input name="email" placeholder="E-mail" type="text" />
                      <input name="password" placeholder="Password..." type="password" />
                      <button type="submit">REGISTER</button>
                    </form>
                    <p>{this.state.registerErrorMessage}</p>
                  </div>
                </div>)
              }
              {this.state.createNewTwat &&
                (<div id="new-message-screen" className="login-screen">
                  <div className="login-box">
                    <form  onSubmit={this.postNewMessage} >
                      <input name="message" placeholder="Write from the heart..." type="text" />
                      <button type="submit">POST</button>
                    </form>
                    <p>{this.state.registerErrorMessage}</p>
                  </div>
                </div>)

              }
              <div>
                <header className="App-header">
                  <img src={logo} className="Lord-logo" alt="logo" />
                  <h1 className="App-title">
                    {title}{this.state.user && this.state.user.nickname && ', ' + this.state.user.nickname}
                  </h1>
                  {!this.state.user ? <div>
                                        <button onClick={()=>this.setState({showLogin:true})} >LOGIN</button> 
                                        <button onClick={()=>this.setState({showRegister:true})}>REGISTER</button>
                                      </div>
                    : <div>
                        <button onClick={this.logout} >LOGOUT</button>
                        <button onClick={()=> this.setState({createNewTwat: true})}>NEW TWAT</button>
                      </div>}
                  <form id="search-form" onSubmit={this.searchTweets}
                    style={{background:'#4d4d4d',display:'flex',alignItems:'center',borderRadius:5}}
                    >
                    <IconButton
                      tooltip="Search" 
                      hoveredStyle={{backgroundColor:'rgba(30,30,30,0.3)'}} 
                      style={{borderRadius:'50%',marginRight:'-15pt',height:65,width:65}}
                      type="submit"
                      >
                      <Search/>
                    </IconButton>
                    <input ref={e=>this.search = e}
                      style={{padding: '0.5em', background:'#4d4d4d',
                        border:'none',outline:'none',caretColor:'#fff',
                        fontSize:'24px', overflow:'auto', flex:1,borderRadius:5, color:'#fff'
                      }} 
                      name="searchvalue" placeholder="You write, I search..." type="text" 
                    />
                    <select name="searchtype" onChange={this.searchTweets} ref={e=>this.searchtype=e}>
                      <option value="owner">User</option>
                      <option value="text">Content</option>
                    </select>
                  </form>
                  <div>
                    <h3 style={{color:'#fff'}}>Sort by:</h3>
                    <button onClick={this.dateSort} >TIME</button>
                    <button onClick={this.fameSort} >POPULARITY</button>
                  </div>
                  
                </header>
                <div className="Item-container">
                  {!this.state.filter ? this.state.tweets.map((tweet,index) =>{ 
                      return (<Item onEditItem={this.saveTweetEdit} key={index} 
                        tweetId={tweet._id}
                        deleteTweet={this.deleteTweet}
                        owner={tweet.owner}
                        hasUpVoted={tweet.hasUpVoted}
                        hasDownVoted={tweet.hasDownVoted}
                        upVote={this.upVote}
                        downVote={this.downVote}
                        cardText={tweet.text}
                        upVotes={tweet.upVotes}
                        downVotes={tweet.downVotes} 
                        currentUser={this.state.user ? this.state.user.nickname : null}
                      />)
                  })
                    : this.state.tweets
                        .filter(tweet => tweet[this.state.filterType].toLowerCase().indexOf(this.state.filter)!==-1)
                        .map((tweet,index) => {
                          return (<Item onEditItem={this.saveTweetEdit} key={index} 
                            tweetId={tweet._id}
                            deleteTweet={this.deleteTweet}
                            owner={tweet.owner}
                            cardText={tweet.text}
                            hasUpVoted={tweet.hasUpVoted}
                            hasDownVoted={tweet.hasDownVoted}
                            upVote={this.upVote}
                            downVote={this.downVote}
                            upVotes={tweet.upVotes}
                            downVotes={tweet.downVotes} 
                            currentUser={this.state.user ? this.state.user.nickname : null}
                        />)})}
                </div>
              </div>
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
