import React from 'react'
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card'
import IconButton from 'material-ui/IconButton'
import avatar from './avatar.jpg'
import ArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down'
import ArrowUp from 'material-ui/svg-icons/hardware/keyboard-arrow-up'
import Delete from 'material-ui/svg-icons/action/delete'

export default class Item extends React.Component {
  itemSelfDelete = () => this.props.deleteTweet(this.props.tweetId)
  upVote = () => this.props.upVote(this.props.tweetId, this.props.hasDownVoted)
  downVote = () => this.props.downVote(this.props.tweetId, this.props.hasUpVoted)
  render() {
    
    return(
      <div className="Card">
        <Card >
          <CardHeader
          
            title={this.props.owner}
            avatar={avatar}
            style={{display:'flex', flexDirection:'row', justifyContent:'flex-start'}}
          >
            {this.props.currentUser && this.props.currentUser === this.props.owner && 
              <div className="Edit-div">
              <IconButton onClick={this.itemSelfDelete} 
                tooltip="Delete post" 
                hoveredStyle={{backgroundColor:'rgba(30,30,30,0.3)'}} 
                style={{borderRadius:'50%', marginTop:-10}}
                >
                <Delete />
              </IconButton>
            </div>}
          </CardHeader>
          <CardText>
            {this.props.cardText}
          </CardText>
          <CardActions style={{justifyContent:'flex-start',display:'flex'}}>
            <div style={{display:'flex',flexDirection:'row',marginRight:40}}>
              <IconButton onClick={this.upVote} 
                disabled={this.props.hasUpVoted || !this.props.currentUser}
                tooltip="Upvote" hoveredStyle={{backgroundColor:'rgba(30,30,30,0.3)'}} 
                style={{borderRadius:'50%', marginRight:-15}}
                >
                <ArrowUp />
              </IconButton>
              <p style={{color:'#6d6d6d'}}><sup>{this.props.upVotes}</sup></p>
            </div>
            <div style={{display:'flex',flexDirection:'row'}}>
              <IconButton onClick={this.downVote} 
                disabled={this.props.hasDownVoted || !this.props.currentUser}
                tooltip="Downvote" hoveredStyle={{backgroundColor:'rgba(30,30,30,0.3)'}} 
                style={{borderRadius:'50%', marginRight:-15}}
                >
                <ArrowDown />
              </IconButton>
              <p style={{color:'#6d6d6d'}}><sup>{this.props.downVotes}</sup></p>
            </div>  
          </CardActions>
        </Card>
      </div>
    )
  }
}