sessions
==========
token (unique)
userId
lastActivity

resets
==========
token (unique)
userId
createdAt

users
==========
userId (auto mongo-id?)
email
nickname
password

tweets
==========
userId
text
upvotes
downvotes
createdAt

POST /register
email
nickname
password
OK -> User + sessionToken
Invalid -> unique/validation errors

POST /login
email
password
OK -> User + sessionToken
Invalid -> validation/auth errors

DELETE /logout
sessionToken

POST /reset-token
email
OK -> skicka mail
Invalid email -> OK respons ändå? Validation errors?

POST /reset
email
resetToken
password
->
OK -> uppdatera lösenord -> User + sessionToken
Invalid -> errors

Login throttling för att skippa brute force.

